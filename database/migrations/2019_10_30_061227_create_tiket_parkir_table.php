<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiketParkirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiket_parkir', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('blok_parkir_id');
            $table->timestamps();
            
            $table->foreign('blok_parkir_id')->references('id')->on('blok_parkir');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiket_parkir');
    }
}
