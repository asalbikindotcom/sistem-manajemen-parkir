<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateToBlokParkirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blok_parkir', function (Blueprint $table) {
            // $table->unsignedBIgInteger('jumlah_kendaraan_id');
            // $table->foreign('jumlah_kendaraan_id')->references('id')->on('jumlah_kendaraan');
            $table->string('kendaraan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blok_parkir', function (Blueprint $table) {
            //
        });
    }
}
