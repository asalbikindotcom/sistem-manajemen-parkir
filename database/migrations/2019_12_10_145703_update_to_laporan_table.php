<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateToLaporanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laporan', function (Blueprint $table) {
            $table->unsignedBigInteger('kasir_id');
            $table->unsignedBIgInteger('tiket_parkir_id');
            
            
            $table->foreign('tiket_parkir_id')->references('id')->on('tiket_parkir');
            $table->foreign('kasir_id')->references('id')->on('kasir');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laporan', function (Blueprint $table) {
            $table->dropColumn('total_pendapatan');
            $table->timestamps();
        });
    }
}
