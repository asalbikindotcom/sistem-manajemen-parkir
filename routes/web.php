<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('logout','\app\Http\Controllers\Auth\LoginController@logout');Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');
	
	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');
	
	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');
	
	Route::get('map', function () {
		return view('pages.map');
	})->name('map');
	
	Route::get('/home','HomeController@loadBP')->name('home');

	Route::get('dataparkir',['as'=>'dataparkir.dashboard_dataparkir','uses'=>'DataParkirController@Dashboard']);
	Route::get('dataparkir/input', array('uses' => 'DataParkirController@FormBlock', 'as' => 'users.parkirinput'));
	Route::post('dataparkir',['as'=>'dataparkir.insert','uses'=>'DataParkirController@Store']);
	Route::get('dataparkir/delete-data/{id}','DataParkirController@delete');
	Route::get('dataparkir/edit-data/{id}',['as'=>'dataparkir.edit','uses'=>'DataParkirController@edit']);
	Route::patch('dataparkir/update/{id}',['as'=>'dataparkir.update','uses'=>'DataParkirController@update']);
	

	Route::get('tiket',['as'=>'tiket.karcis','uses'=>'TiketController@TiketPrint']);
	Route::get('tiket/vcard',['as'=>'tiket.vcard','uses'=>'TiketController@vcard']);
	// Route::get('tiket/vcard','TiketController@vcard');

	Route::get('kasir',['as'=>'kasir.dashboard','uses'=>'KasirController@index']);
	Route::post('kasir',['as'=>'kasir.store','uses'=>'KasirController@Store']);

	Route::get('laporan','LaporanController@index');
	Route::get('laporan',['as'=>'laporan.laporanHarian','uses'=>'LaporanController@lapHarian']);
	Route::get('laporan/cetak_pdf','LaporanController@cetak_pdf');
	
	
});


Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

