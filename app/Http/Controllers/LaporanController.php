<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use PDF;
use App\kasir;
use App\laporan;

class LaporanController extends Controller
{
    //
    public function index(){

        $kasirdata=kasir::all();
        return view('laporan.laporanHarian',['kasirdata'=>$kasirdata]);
    }

    public function LapHarian(Request $request){
        $kasirdata=DB::table('kasir')->get();
        
        // dd($kasirdata);
        // return view('laporan.laporanHarian',$kasirdata);
        return view('laporan.laporanHarian',compact('kasirdata'));
    }

    public function cetak_pdf(){
        $kasirdata=kasir::all();
        $pdf=PDF::loadview('laporan.cetak_pdf_laporan',['kasirdata'=>$kasirdata]);
        return $pdf->download('laporan harian.pdf');
        // return $pdf->stream();
        
    }
}
