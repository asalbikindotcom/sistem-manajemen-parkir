<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\dataparkir;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        

        return view('dashboard');
    }

    public function loadBP(Request $request){

        $loadBP=DB::table('blok_parkir')->get();
// dd($loadBP);
        return view('dashboard',compact('loadBP'));
    }
}
