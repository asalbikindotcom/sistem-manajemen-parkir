<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use QRCode;

class TiketController extends Controller
{
    //
    public function TiketPrint(){
        $file = public_path('qr_code.png');
        return QRCode::text('QR Code Generator for Laravel!')->setOutfile($file)->png(); 
    }
    public function vcard(){
        // Personal Information
        return QRCode::meCard('John Doe', '1234 Main st.', '+1 001 555-1234', 'john.doe@example.com')->svg();
        // $code = QRCode::meCard('John Doe', '1234 Main st.', '+1 001 555-1234', 'john.doe@example.com')->svg();
        return view('tiket.karcis',compact('code',$code));
    }
}
