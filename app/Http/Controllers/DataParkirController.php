<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\dataparkir;

class DataParkirController extends Controller
{
    public function Dashboard(Request $request){
        $loadBP=DB::table('blok_parkir')->get();

        return view('dataparkir.dashboard_dataparkir',compact('loadBP'));
    }
    public function FormBlock(){
        return view('dataparkir.form_input');
    }
    public function Store(Request $request){
        $data=new dataparkir();
        $data->nama_blok=$request->get('nama_blok');
        $data->kapasitas=$request->get('kapasitas');
        $data->kendaraan=$request->get('kendaraan');

        $data->save();
        return redirect('/dataparkir')->withStatus(__('Data berhasil ditambahkan'));
        
    }

    public function delete($id){
        
        $data=DB::table('blok_parkir')->where('id',$id)->delete();
        return redirect()->back()->withStatus(__('Data berhasil dihapus'));
    }

    public function edit($id){

        $data = dataparkir::find($id);
        $blokparkir=dataparkir::all();
        return view('dataparkir.form_edit',compact('data','blokparkir'));
        // return view('dataparkir.form_edit',['data',$data]);
        // dd($data);
    }

    public function update($id){
        $data=dataparkir::find($id);

        $data->update([
            'nama_blok'=>request('nama_blok'),
            'kapasitas'=>request('kapasitas'),
            'kendaraan'=>request('kendaraan'),
        ]);

        // return redirect()->route('dataparkir.update');
        return redirect('dataparkir')->withStatus(__('Data berhasil diupdate'));
    }
    

}
