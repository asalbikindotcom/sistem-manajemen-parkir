<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\kasir;


class KasirController extends Controller
{
    //
    public function index(){

         return view('kasir.dashboard');
    }

    public function Store(Request $request){
        
        $data=new kasir();
        $data->plat_nomor=$request->get('plat_nomor');
        $data->harga=$request->get('harga');
        $data->denda=$request->get('denda');
        $data->jam_masuk=$request->get('jam_masuk');
        $data->jam_keluar=$request->get('jam_keluar');
        $data->kendaraan=$request->get('kendaraan');
        
        $data->save();

        return redirect()->back()->withStatus(__('Data berhasil ditambahkan'));
        // return redirect()->route('kasir.dashboard');
        // return $request->all();
    }
}
