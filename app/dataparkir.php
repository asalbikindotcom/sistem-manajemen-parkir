<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dataparkir extends Model
{
    protected $table = "blok_parkir";
    protected $fillable = ['nama_blok','kapasitas','kendaraan'];
}
