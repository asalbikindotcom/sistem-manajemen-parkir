@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
              <div class="card-icon">
                <i class="material-icons">keyboard_arrow_right</i>
              </div>
              <p class="card-category">Kendaraan masuk</p>
              <h3 class="card-title">50
                {{-- <small>GB</small> --}}
              </h3>
            </div>
            <div class="card-footer">
              {{-- <div class="stats">
                <i class="material-icons text-danger">warning</i>
                <a href="">Get More Space...</a>
              </div> --}}
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="material-icons">keyboard_arrow_left</i>
              </div>
              <p class="card-category">Kendaraan keluar</p>
              <h3 class="card-title">245</h3>
            </div>
            <div class="card-footer">
              {{-- <div class="stats">
                <i class="material-icons">date_range</i> Last 24 Hours
              </div> --}}
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
              <div class="card-icon">
                <i class="material-icons">info_outline</i>
              </div>
              <p class="card-category">Blacklist</p>
              <h3 class="card-title">75</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                {{-- <i class="material-icons">local_offer</i> Tracked from Github --}}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="material-icons">directions_car</i>
              </div>
              <p class="card-category">Blok parkir</p>
              <h3 class="card-title">25</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                {{-- <i class="material-icons">update</i> Just Updated --}}
              </div>
            </div>
          </div>
        </div>
      </div>
      {{-- {{$loadBP}} --}}
      {{-- @foreach($loadBP as $loadBP)
      {{$loadBP}}
      @endforeach --}}
      {{-- tabel blok --}}
      {{-- <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-footer"></div>
          <tr>
            <th><h2 class="card-title" align="center">BLOCK A </h2></th>
            <div class="card-footer"></div>
            <td><h3 class="card-title" align="center">JUMLAH PARKIR : 30/100</h3></td>
            <div class="card-footer"></div>
            <td><h3 class="card-title" align="center">KAPASITAS : 100</h3></td>
            <div class="card-footer"></div>
          </tr>
          </div>
        </div>
      </div> --}}
      {{-- end table --}}
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title ">Data parkir</h4>
          <p class="card-category"> Table Block Parkir</p>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <th>
                  No
                </th>
                <th>
                  Block Parkir
                </th>
                <th>
                  Kapasitas
                </th>
                <th>
                  Kendaraan
                </th>
                <th>

                </th>
              </thead>
              <tbody>
                  @php $no = 1; @endphp
                @foreach($loadBP as $send)
                <tr>
                  <td>
                    {{$no++}}
                  </td>
                  <td>
                    {{ $send->nama_blok }}
                  </td>
                  <td>
                      {{ $send->kapasitas }}
                  </td>
                  <td>
                    {{$send->kendaraan}}
                  </td>
                  <td>
                      
                    
                    </td>
                  </td>
                </tr>
                @endforeach()
              </tbody>
              {{-- <button type="button" class="btn btn-sucess" onclick="window.location='{{ route("users.parkirinput") }}'">Tambah Block</button> --}}
              
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();
    });
  </script>
@endpush