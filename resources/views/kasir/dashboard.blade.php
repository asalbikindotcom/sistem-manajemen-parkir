@extends('layouts.app', ['activePage' => 'icons', 'titlePage' => __('Kasir')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          {{-- <form method="post" action="{{ route('profile.update') }}" autocomplete="off" class="form-horizontal"> --}}
          <form method="post" action="{{route('kasir.store')}}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Dashboard Kasir') }}</h4>
                <p class="card-category">{{ __('Kasir Sistem Parkir') }}</p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Plat Nomor') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                        <input class="form-control" type="text" placeholder="AB 7534 DU" name="plat_nomor"/>            
                    </div>
                  </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Harga') }}</label>
                    <div class="col-sm-7">
                      <div class="form-group">
                          <input class="form-control" type="text" placeholder="Rp." name="harga"/>            
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Denda') }}</label>
                    <div class="col-sm-7">
                      <div class="form-group">
                          <input class="form-control" type="text" placeholder="Rp." name="denda"/>           
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Jam masuk ') }}</label>
                    <div class="col-sm-7">
                      <div class="form-group">
                          {{-- <input type="text" class="form-control datetimepicker" value="21/06/2018"/> --}}
                          <input class="form-control" type="text" name="jam_masuk" placeholder="19:00">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Jam keluar') }}</label>
                    <div class="col-sm-7">
                      <div class="form-group">
                          <input class="form-control" type="text" name="jam_keluar" placeholder="21:00"/>            
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label">{{ __('Jenis Kendaraan') }}</label>
                      <div class="col-sm-7">
                        <div class="form-group">
                          <input class="form-control-input" type="radio" value="Mobil" name="kendaraan">
                        <span>Mobil</span>  
                      </div>
                    <div class="form-group">
                      <input class="form-control-input" type="radio" value="Motor" name="kendaraan">
                      <span>Motor</span>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

@endsection
  
@push('js')
<script>
  $('.datetimepicker').datetimepicker({
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
    }
});
</script>
@endpush
