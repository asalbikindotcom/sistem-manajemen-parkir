@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('Data parkir')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('dataparkir.update',$data->id) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('PATCH')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">Data parkir</h4>
                <p class="card-category"> Update Block Parkir</p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nama block') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                    <input class="form-control" name="nama_blok" type="text" placeholder="{{ __('Block X') }}" value="{{$data->nama_blok}}" aria-required="true"/>
                        <span id="name-error" class="error text-danger" for="input-name"></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Kapasitas') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                    <input class="form-control" name="kapasitas" type="text" placeholder="{{ __('90/100') }}" value="{{$data->kapasitas}}" aria-required="true"/>
                        <span id="name-error" class="error text-danger" for="input-name"></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Jenis Kendaraan') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control-input" type="radio" name="kendaraan" value="Mobil">
                    <span>Mobil</span>  
                  </div>
                <div class="form-group">
                <input class="form-control-input" type="radio"  name="kendaraan" value="Motor">
                  <span>Motor</span>
                </div>
              </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>
  </div>
@endsection