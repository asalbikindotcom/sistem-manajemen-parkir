@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('Data parkir')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        
        
      {{-- end button --}}
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Data parkir</h4>
            <p class="card-category"> Table Block Parkir</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    No
                  </th>
                  <th>
                    Block Parkir
                  </th>
                  <th>
                    Kapasitas
                  </th>
                  <th>
                    Kendaraan
                  </th>
                  <th>

                  </th>
                </thead>
                <tbody>
                    @php $no = 1; @endphp
                  @foreach($loadBP as $send)
                  <tr>
                    <td>
                      {{$no++}}
                    </td>
                    <td>
                      {{ $send->nama_blok }}
                    </td>
                    <td>
                        {{ $send->kapasitas }}
                    </td>
                    <td>
                      {{$send->kendaraan}}
                    </td>
                    <td>
                        <a href="dataparkir/edit-data/{{$send->id}}" class="btn btn-primary">Edit</a>
                        <a href="dataparkir/delete-data/{{ $send->id }}" class="btn btn-danger" onclick="return confirm('Are you sure to delete?')">Hapus</a>
                      
                      </td>
                    </td>
                  </tr>
                  @endforeach()
                </tbody>
                <button type="button" class="btn btn-sucess" onclick="window.location='{{ route("users.parkirinput") }}'">Tambah Block</button>
                
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>
@endsection