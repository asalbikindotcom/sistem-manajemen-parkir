<html>
<head>
	<title>Laporan Harian Sistem Parkir</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Harian Sistem Parkir</h4>
		
	</center>

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>plat nomor</th>
				<th>kendaraan</th>
				<th>jam masuk</th>
				<th>jam keluar</th>
                <th>harga</th>
                <th>denda</th>
                <th>total bayar</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($kasirdata as $p)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$p->plat_nomor}}</td>
                <td>{{$p->kendaraan}}</td>
                <td>{{$p->jam_masuk}}</td>
                <td>{{$p->jam_keluar}}</td>
                <td>{{$p->harga}}</td>
                <td>{{$p->denda}}</td>
                <td>{{(($p->harga)+($p->denda))}}</td>
				
			</tr>
			@endforeach
		</tbody>
	</table>

</body>
</html>