@extends('layouts.app', ['activePage' => 'map', 'titlePage' => __('Laporan')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        
        
      {{-- end button --}}
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Data Laporan</h4>
            <p class="card-category"> Tabel laporan harian sistem parkir</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    No
                  </th>
                  <th>
                    Plat Nomor
                  </th>
                  <th>
                    Jenis Kendaraan
                  </th>
                  <th>
                    Parkir Masuk
                  </th>
                  <th>
                    Parkir Keluar
                  </th>
                  <th>
                    Tarif
                  </th>
                  <th>
                    Denda
                  </th>
                  <th>
                    Total Bayar
                  </th>
                </thead>
                <tbody>
                    @php $no = 1; @endphp
                  @foreach ($kasirdata as $send)
                  <tr>
                    <td>
                      {{$no++}}
                    </td>
                    <td>
                      {{ $send->plat_nomor }}
                    </td>
                    <td>
                      {{ $send->kendaraan }}
                    </td>
                    <td>
                      {{ $send->jam_masuk}}
                    </td>
                    <td>
                        {{ $send->jam_keluar}}
                    </td> 
                    <td class="text-primary">
                      Rp.{{ $send->harga}}
                    </td>
                    <td class="text-primary">
                      Rp.{{ $send->denda}}
                    </td>
                    <td class="text-primary">
                      Rp.{{ (($send->harga)+($send->denda)) }}
                    </td> 
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <a href="laporan/cetak_pdf" class="btn btn-primary" target="_blank">CETAK PDF</a>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>
@endsection