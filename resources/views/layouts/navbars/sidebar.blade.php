<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  {{-- logone  --}}
  <div class="logo">
    <a href="" class="simple-text logo-normal">
      {{ __('Sistem Parkir') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      {{-- <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? 'active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="true">
          <i class="material-icons">account_box</i>
          <p>{{ __('Setting User') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="laravelExample">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">     
              <a class="nav-link" href="{{ route('profile.edit') }}">
                <span class="sidebar-mini"> UP </span>
                <span class="sidebar-normal">{{ __('User profile') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('user.index') }}">
                <span class="sidebar-mini"> UM </span>
                <span class="sidebar-normal"> {{ __('User Management') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li> --}}
      <li class="nav-item{{ $activePage == 'table' ? ' active' : '' }}">
      <a class="nav-link" href="{{route('dataparkir.dashboard_dataparkir')}}">
        {{-- <a class="nav-link" href="{{ route('table') }}"> --}}
          <i class="material-icons">time_to_leave</i>
            <p>{{ __('Data Block Parkir') }}</p>
        </a>
      </li>
      {{-- <li class="nav-item{{ $activePage == 'typography' ? ' active' : '' }}">
      <a class="nav-link" href="{{ route('tiket.karcis')}}">
        <a class="nav-link" href="{{ route('tiket.vcard')}}">
        <a class="nav-link" href="{{ route('typography') }}">
          <i class="material-icons">library_books</i>
            <p>{{ __('Tiket') }}</p>
        </a>
      </li> --}}
      <li class="nav-item{{ $activePage == 'icons' ? ' active' : '' }}">
      <a class="nav-link" href="{{ route('kasir.dashboard') }}">        
        {{-- <a class="nav-link" href="{{ route('icons') }}"> --}}
          <i class="material-icons">payment</i>
          <p>{{ __('Kasir') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'map' ? ' active' : '' }}">
      <a class="nav-link" href=" {{ route('laporan.laporanHarian')}}">
        {{-- <a class="nav-link" href="{{ route('map') }}"> --}}
          <i class="material-icons">print</i>
            <p>{{ __('Laporan') }}</p>
        </a>
      </li>
      {{-- <li class="nav-item{{ $activePage == 'notifications' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('notifications') }}">
          <i class="material-icons">notifications</i>
          <p>{{ __('Notifications') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'language' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('language') }}">
          <i class="material-icons">language</i>
          <p>{{ __('RTL Support') }}</p>
        </a>
      </li> --}}
      {{-- <li class="nav-item active-pro{{ $activePage == 'upgrade' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('upgrade') }}">
          <i class="material-icons">unarchive</i>
          <p>{{ __('Upgrade to PRO') }}</p>
        </a>
      </li> --}}
    </ul>
  </div>
</div>